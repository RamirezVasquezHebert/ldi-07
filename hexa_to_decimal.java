
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hebert
 */
public class hexa_to_decimal {
    
    
    public static void main(String args[]) {
        
          String[] a1= Dividir(JOptionPane.showInputDialog("Ingrese el número en Hexadecimal"));
          System.out.print("Valor en Hexadecimal ingresado ");
           for (String item : a1) {
             System.out.print(item);
         }  
           
           System.out.println();
          String val=HexaADecimal(a1[0]);                    
          
          if(Integer.parseInt(val)<=7){
              
              String corte = Unir(a1);
              
              String resultado =HexaADecimal(corte);
              
              System.out.println("Resultado en decimal " + resultado);
          }else {          
              
            String corte1 = Unir(a1);
              
              String resultado1 =HexaADecimal(corte1);
              
              System.out.println("Resultado en decimal -" + resultado1);
          }
    }
    
     public static String[] Dividir(String a) {
           String[] arreglo = new String[a.length()];
          int cadena = a.length();
          String temp = "";
         
          int x = 0;
          for (int i = 0; i < cadena; i++) {  
              
           
             
              temp += a.substring(x,x+1);
           
           
            arreglo[i]=temp;
           temp = "";
          x+=1;
           }
            
         
         return arreglo;
     }
    
     
     public static String DecimalAHexa(String a) {
        int x = Integer.parseInt(a);
        String resultado = Integer.toString(x, 16);
        return resultado;
    }
     
     
    public static String HexaADecimal(String a) {
        int a1 = Integer.parseInt(a, 16);
        String resultado = a1 + "";
        return resultado;
    }
     
    
    public static String Unir(String[] a){
        String c = "";
         for (int i = 1;i<a.length;  i++){
                c = c + a[i];
           }
        
        return c;
    }
    
}
