
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author hebert
 */
public class Complemento_a_2_hexa {
    
    
    public static void main(String arg[]){ 
        
        
        String [] a1 = Dividir(JOptionPane.showInputDialog("Ingrese el número Hexadecimal"));
        System.out.print("Número ingresado "); 
     
        for (String item : a1) {
             System.out.print(item);
         }   
     
        System.out.println();
     
        System.out.print("Número invertido "); 
         String [] inv = Invertir (a1);
       
         for (String item : inv) {
             System.out.print(item);
         }   

        System.out.println();
        
        String Complemento = Complementar(inv);
         System.out.print("Número Implementado " + Complemento); 
            System.out.println();
        
    }
    
     public static String DecimalAHexa(String a) {
        int x = Integer.parseInt(a);
        String resultado = Integer.toString(x, 16);
        return resultado;
    }
     
     
      public static String HexaADecimal(String a) {
        int a1 = Integer.parseInt(a, 16);
        String resultado = a1 + "";
        return resultado;
    }
      
      public static String[] Dividir(String a) {
           String[] arreglo = new String[a.length()];
          int cadena = a.length();
          String temp = "";
         
          int x = 0;
          for (int i = 0; i < cadena; i++) {  
              
           
             
              temp += a.substring(x,x+1);
           
           
            arreglo[i]=temp;
           temp = "";
          x+=1;
           }
            
         
         return arreglo;
     }
      
      
      public static String[] Invertir(String a[]) {
         
          String [] x = new String [a.length];
                  
          String c = "";
          String b = "";
        for (int i =  a.length - 1; i > -1; i--) {            
            String decimal = HexaADecimal(a[i]);
             int aux = 15- Integer.parseInt(decimal);
             c = c + aux;
            String hexa = DecimalAHexa(c);
           x[i]=hexa;
           
           c = "";
           }
            
          
        return x;
      }
      
      
       public static String Complementar(String a[]) {
        
           String comp = "";
          
           
           for(int i=0;i<a.length;i++){
                comp = comp + a[i];
           }
            String comp1 = HexaADecimal(comp);
            int x = Integer.parseInt(comp1) ;
            x+=1;
            String comp2 = Integer.toString(x);
            String comp_final = DecimalAHexa(comp2); 
                        
           return comp_final;
       }
      
       
}
